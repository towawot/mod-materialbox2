Scriptname aaatowMB2_Auto_Put_In_Alias extends ReferenceAlias  

aaatowMB2_Var_Quest Property MB2 Auto
import aaatowMB2

Formlist list

Event OnInit()
	if !GetOwningQuest().IsRunning()
		return
	endif
	list = MB2.flMaterial.Getat(self.GetID()) as formlist
	RegisterForMenu("Crafting Menu")
endEvent

Event OnPlayerLoadGame()
	list = MB2.flMaterial.Getat(self.GetID()) as formlist
	RegisterForMenu("Crafting Menu")
endEvent

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)

	if !akBaseItem
		return
	endif
; 	if  !akBaseItem.HasKeyword(kwd)
; 		return
; 	endif
	if !list.HasForm(akBaseItem)
		return
	endif

; 	if !GetIsType(akBaseItem)
; ; 		debug.Notification("name: "+ akBaseItem.GetName() + "  Gettype: "+akBaseItem.GetType())
; 		return
; 	endif

	if AliasSearch(MB2.ContainerManagerQuest, akSourceContainer) != -1
		return
	endif
 
	if IsQuestItem(Game.GetPlayer(), akBaseItem)
		return
	endif

	int ID = self.GetID()
	if MB2.State2[ID]
; 		debug.Notification("name: "+ akBaseItem.GetName() + "  MB2.State2["+ID+"]: "+MB2.State2[ID])
		ObjectReference ContainerObject = (MB2.ContainerManagerQuest.GetAlias(ID) as ReferenceAlias).GetReference()
		Game.GetPlayer().RemoveItem(akBaseItem, aiItemCount, true, ContainerObject)
	endif
; 	if MB2.fLearningItem as bool
; 		if !list.HasForm(akBaseItem)
; ; 			debug.Notification(kwd.GetID() as string + "addform")
; 			list.AddForm(akBaseItem)
; 		endif
; 	endif
endEvent

Event OnMenuOpen(String MenuName)
	if MenuName == "Crafting Menu"
		gotoState("Craft")
	endif
endEvent

Event OnMenuClose(String MenuName)
endEvent
	
State Craft
	Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	endEvent

	Event OnMenuOpen(String MenuName)
	endEvent

	Event OnMenuClose(String MenuName)
		if MenuName == "Crafting Menu"
			gotoState("")
		endif
	endEvent
endState


Int Function AliasSearch(Quest Qst, ObjectReference ref)
	int max = Qst.GetNumAliases()
	int index = 0
	while index < max
		if (Qst.GetAlias(index) as ReferenceAlias).GetReference() == ref
			return index
		endif
		index += 1
	endWhile
	return -1
endFunction

;  bool Function GetIsType(form akBase)
;  	if akBase.GetType() == 30	; ingredient
;  		return true
;  	elseif akBase.GetType() == 32	; misc
;  		return true
;  	elseif akBase.GetType() == 46	; potion
;  		return true
;  	elseif akBase.GetType() == 52	; soulgem
;  		return true
;  	endif
;  	return false
;  endFunction; 