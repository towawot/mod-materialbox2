;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname aaatowMB2_ContainerMenu_Perk Extends Perk Hidden

Import Utility
aaatowMB2_Var_Quest Property MB2 Auto
ObjectReference orObj
ObjectReference orDeste

;BEGIN FRAGMENT Fragment_2
Function Fragment_2(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
	Message MessageMenu
	int MenuNumber = 1
	int MessageSelect
	int Max = MB2.flMessageMenu.GetSize()

	if AliasSearch(MB2.ContainerManagerQuest, akTargetRef) != -1
		;it's materialbox
		Debug.Notification("it's materialbox ")
		orObj = akTargetRef
		orDeste = Game.GetPlayer()
	else
		Debug.Notification("no materialbox ")
		orObj = akTargetRef
		orDeste = (MB2.ContainerManagerQuest.GetAlias(0) as ReferenceAlias).GetReference()
	endif

	While MenuNumber < Max
		MessageMenu = MB2.flMessageMenu.Getat(MenuNumber) as Message
		MessageSelect = MessageMenu.Show()
		MenuNumber = SelectCheck(MenuNumber, MessageSelect)
	endWhile
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Int Function AliasSearch(Quest Qst, ObjectReference ref)
	int max = Qst.GetNumAliases()
	int index = 0
	while index < max
		if (Qst.GetAlias(index) as ReferenceAlias).GetReference() == ref
			return index
		endif
		index += 1
	endWhile
	return -1
endFunction

Int Function SelectCheck(Int _Menu, Int _Select)
	if _Menu == 1
		if _Select == 0	;open
			orObj.Activate(Game.GetPlayer())
			return 100
		elseif _Select == 1	;putin
			return 2
		elseif _Select == 2	;takeout
			return 2
		else
			return 100
		endif
	elseif _Menu == 2
		if _Select == 0	;next
			return 3
		elseif _Select == 1	;all
			
		endif
	endif
	return 100
endFunction
