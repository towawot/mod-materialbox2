Scriptname aaatowMB2_Container_Alias extends ReferenceAlias  

aaatowMB2_Var_Quest Property MB2 Auto
import aaatowMB2

Weapon wpn
bool bRunWeightAlias = false

Event OnInit()
	if !GetOwningQuest().IsRunning()
		return
	endif

	MB2.flDuplicateConfirm.revert()

	if (GetOwningQuest() == MB2.ContainerManagerQuest || GetOwningQuest() == MB2.WeightCalculationQuest[0])
		formlist list = MB2.flMaterial.Getat(self.GetID()) as formlist
		AddInventoryEventFilter(list)
	elseif GetOwningQuest() == MB2.WeightCalculationQuest[1]
		Utility.Wait(self.GetID())
		ObjectReference orObj = Self.GetReference()
		if !MB2.flDuplicateConfirm.HasForm(orObj)
			MB2.flDuplicateConfirm.AddForm(orObj)
			bRunWeightAlias = true
		else
			bRunWeightAlias = false
		endif
	endif
	if (GetOwningQuest() == MB2.WeightCalculationQuest[0] || GetOwningQuest() == MB2.WeightCalculationQuest[1])
		wpn = MB2.flWeightWeapon.Getat(0) as Weapon
	endif
endEvent

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	if IsQuestItem(Self.GetReference(), akBaseItem)
		self.GetReference().RemoveItem(akBaseItem, aiItemCount, true, akSourceContainer)
	endif

	if (GetOwningQuest() == MB2.WeightCalculationQuest[1])
		if !bRunWeightAlias
			return
		elseif MB2.HasMaterialList(akBaseItem, False)
			return
		endif
	endif

	if (GetOwningQuest() == MB2.WeightCalculationQuest[0] || GetOwningQuest() == MB2.WeightCalculationQuest[1])
		float weight = akBaseItem.GetWeight()
		if weight
			int ItemCount = ((weight * 10) * aiItemCount) as int
			Game.GetPlayer().AddItem(wpn, ItemCount, true)
		endif
	endif
endEvent

Event OnItemRemoved(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
	if (GetOwningQuest() == MB2.WeightCalculationQuest[1])
		if !bRunWeightAlias
			return
		elseif MB2.HasMaterialList(akBaseItem, False)
			return
		endif
	endif
	
	if (GetOwningQuest() == MB2.WeightCalculationQuest[0] || GetOwningQuest() == MB2.WeightCalculationQuest[1])
		float weight = akBaseItem.GetWeight()
		if weight
			int ItemCount = ((weight * 10) * aiItemCount) as int
			Game.GetPlayer().RemoveItem(wpn, ItemCount, true)
		endif
	endif
endEvent
