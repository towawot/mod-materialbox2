Scriptname aaatowMB2_Control_Power_Mgef extends ActiveMagicEffect  

Quest Property WorkQuest Auto
aaatowMB2_Var_Quest Property MB2 Auto
int AliasId = 0

ObjectReference Property ContainerMarker Auto


Event OnEffectStart(Actor akTarget, Actor akCaster)
	if akCaster != Game.GetPlayer()
		return
	endif

	if WorkQuest == MB2.PutInQuest
		debug.Notification("put in")
		MB2.PutInQuest.Start()
	elseif WorkQuest == MB2.TakeOutQuest
		debug.Notification("take out")
		MB2.TakeOutQuest.Start()
	elseif WorkQuest == MB2.CallContainerQuest
		ObjectReference ContainerObject = (MB2.ContainerManagerQuest.GetAlias(AliasId) as ReferenceAlias).GetReference()
		ContainerObject.MoveTo(akCaster, 120.0 * Math.Sin(akCaster.GetAngleZ()), 120.0 * Math.Cos(akCaster.GetAngleZ()), akCaster.GetHeight() - 35.0, false)
		float zOffset = ContainerObject.GetHeadingAngle(akCaster)
		ContainerObject.SetAngle(0.0, 0.0, ContainerObject.GetAngleZ() + zOffset)
		akCaster.AddPerk(MB2.ContainerMenuPerk)
	else
		ObjectReference ContainerObject = (MB2.ContainerManagerQuest.GetAlias(AliasId) as ReferenceAlias).GetReference()
		ContainerObject.Activate(akCaster)
	endif
endEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	if WorkQuest == MB2.CallContainerQuest
		Utility.wait(1)
		((MB2.ContainerManagerQuest.GetAlias(AliasId) as ReferenceAlias).GetReference()).MoveTo(ContainerMarker)
		akCaster.RemovePerk(MB2.ContainerMenuPerk)
	endif
endEvent
