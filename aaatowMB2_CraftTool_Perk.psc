;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname aaatowMB2_CraftTool_Perk Extends Perk Hidden

Import Utility
aaatowMB2_Var_Quest Property MB2 Auto

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
RegisterForMenu("Crafting Menu")
Game.GetPlayer().AddSpell(MB2.MaxWeightIncrease, False)

; debug.Notification("Takeout")
MB2.TakeOutQuest.Start()
WaitQuestClose(MB2.TakeOutQuest)

akTargetRef.Activate(Game.GetPlayer())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Function WaitQuestClose(Quest Qst)
	int index = 10
	bool bLoop = True

	wait(0.1)
	While Qst.IsRunning() && bLoop
		wait(0.1)
		index -= 1
		if index
			bLoop = False
		endif
	endWhile
endFunction

Event OnMenuClose(String MenuName)
	if MenuName == "Crafting Menu"
		MB2.PutInQuest.Start()
		WaitQuestClose(MB2.PutInQuest)
		Game.GetPlayer().RemoveSpell(MB2.MaxWeightIncrease)
		UnRegisterForMenu("Crafting Menu")
	endif
endEvent
