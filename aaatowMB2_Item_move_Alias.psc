Scriptname aaatowMB2_Item_move_Alias extends ReferenceAlias  

aaatowMB2_Var_Quest Property MB2 Auto

Event OnInIt()
	if !GetOwningQuest().IsRunning()
		return
	endif
	int ID = self.GetID()
	if !MB2.State2[ID]
		(GetOwningQuest() as aaatowMB2_Stop_Quest).SetCount()
		return
	endif
	
	Actor aPlayer = Game.GetPlayer()
	ObjectReference orObj = self.GetReference()
	formlist list = MB2.flMaterial.Getat(ID) as formlist
	if GetOwningQuest() == MB2.PutInQuest
		if aPlayer.GetItemCount(list) > 0
			aPlayer.RemoveItem(list, 9999, true, orObj)
		endif
	elseif GetOwningQuest() == MB2.TakeOutQuest
		if orObj.GetItemCount(list) > 0
		 	orObj.RemoveItem(list, 9999, true, aPlayer)
		endif
	endif
	(GetOwningQuest() as aaatowMB2_Stop_Quest).SetCount()
endEvent
