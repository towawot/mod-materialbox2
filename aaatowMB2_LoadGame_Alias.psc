Scriptname aaatowMB2_LoadGame_Alias extends ReferenceAlias  

aaatowMB2_Var_Quest Property MB2 Auto
Import Game

Event OnInit()
	Game.GetPlayer().AddPerk(MB2.ActivateCraftFurniture)
	Game.GetPlayer().AddSpell(MB2.flControlSpells.GetAt(2) as Spell)
	s101BugsName = Get101BugsEspName()
	sImprovedFishName = GetImprovedFishEspName()
	CompatibilityChecker()
endEvent

Event OnPlayerLoadGame()
	if MB2.State1[0] && !Game.GetPlayer().HasPerk(MB2.ActivateCraftFurniture)
		Game.GetPlayer().AddPerk(MB2.ActivateCraftFurniture)
	endif
	s101BugsName = Get101BugsEspName()
	sImprovedFishName = GetImprovedFishEspName()
	MB2.Maintenance()
	bool bOK = CopyList(MB2.flUserlist)
	ResetList()
	if bOK
		RecreateUserlist(MB2.flUserlist)
	endif
	CompatibilityChecker()
endEvent

Bool property bDG hidden
	bool Function Get()
		Return (GetModByName("Dawnguard.esm") < 255) as Bool
	EndFunction
EndProperty

Bool property bDB hidden
	bool Function Get()
		Return (GetModByName("Dragonborn.esm") < 255) as Bool
	EndFunction
EndProperty

Bool property bHF hidden
	bool Function Get()
		Return (GetModByName("HearthFires.esm") < 255) as Bool
	EndFunction
EndProperty

string s101BugsName
Bool property b101BUGLowResNormal hidden
	bool Function Get()
		Return (GetModByName("83Willows_101BUGS_V4_LowRes.esp") < 255) as Bool
	EndFunction
EndProperty

Bool property b101BUGHighResNormal hidden
	bool Function Get()
		Return (GetModByName("83Willows_101BUGS_V4_HighRes.esp") < 255) as Bool
	EndFunction
EndProperty

Bool property b101BUGLowResHishSpawn hidden
	bool Function Get()
		Return (GetModByName("83Willows_101BUGS_V4_LowerRes_HighSpawn.esp") < 255) as Bool
	EndFunction
EndProperty

Bool property b101BUGHighResHishSpawn hidden
	bool Function Get()
		Return (GetModByName("83Willows_101BUGS_V4_HighRes_HighSpawn.esp") < 255) as Bool
	EndFunction
EndProperty


string sImprovedFishName

Bool property bImprovedFish hidden
	bool Function Get()
		Return (GetModByName("ImprovedFish.esp") < 255) as Bool
	EndFunction
EndProperty

Bool property bImprovedFishBasic hidden
	bool Function Get()
		Return (GetModByName("ImprovedFishBASIC.esp") < 255) as Bool
	EndFunction
EndProperty

String Function Get101BugsEspName()
	String s
	if b101BUGLowResHishSpawn
		s = "83Willows_101BUGS_V4_LowerRes_HighSpawn.esp"
	elseif b101BUGHighResHishSpawn
		s = "83Willows_101BUGS_V4_HighRes_HighSpawn.esp"
	elseif b101BUGLowResNormal
		s = "83Willows_101BUGS_V4_LowRes.esp"
	elseif b101BUGHighResNormal
		s = "83Willows_101BUGS_V4_HighRes.esp"
	else
		s = ""
	endif
	Return s
endFunction


String Function GetImprovedFishEspName()
	String s
	if bImprovedFish
		s = "ImprovedFish.esp"
	elseif bImprovedFishBasic
		s = "ImprovedFishBASIC.esp"
	else
		s = ""
	endif
	Return s
endFunction

bool Function CopyList(FormList[] list)
	bool bResult = false
	int index = 0
	int max = list[0].GetSize()
	if max as bool
		if max > 128
			max = 127
		endif
		while index <= max
			list[1].AddForm(list[0].GetAt(index))
			index += 1
			bResult = true
		endWhile
	endif
	return bResult
EndFunction

Function CompatibilityChecker()
	if bDG
		SetList("DG")
	endif

	if bDB
		SetList("DB")
	endif

	if bHF
		SetList("HF")
	endif

	if s101BugsName as Bool
		SetList("101BUG")
	endif

	if sImprovedFishName as Bool
		SetList("IFish")
	endif

	SetList("Skyrim")
endFunction

Function ResetList()
	int index = MB2.flMaterial.GetSize()
	Formlist FLST
	while index
		index -= 1
		FLST = MB2.flMaterial.GetAt(index) as Formlist
		FLST.Revert()
	endWhile
endFunction

Function RecreateUserlist(FormList[] list)
	int index = 0
	int max = list[1].GetSize()
	While index < max
		int formid = list[1].Getat(index).GetFormId()
		string modname = GetModName(Math.RightShift(formid, 24))
		if GetModByName(modname) < 255
			form akForm = GetForm(formid)
			if akForm
				list[0].AddForm(akForm)
			endif
		endif
; 		Debug.Notification(modname + "  " + GetForm(formid).GetName())
		index += 1
	endWhile
endFunction


Function SetList(String ModName)
	int index = MB2.flMaterial.GetSize()
	Formlist FLST
	Int _Ingredient = 0
	Int _OreIngot = 1
	Int _AnimalHide = 2
	Int _AnimalPart = 3
	Int _Jewel = 4
	Int _Soulgem = 5
	Int _Drink = 6
	Int _Food = 7
	Int _Potion = 8
	Int _Poison = 9
	Int _FireWood = 10
	Int _Clutter = 11
	Int _Scroll = 12
; 	Int _Userlist = 13
	
	while index
		index -= 1
		FLST = MB2.flMaterial.GetAt(index) as Formlist
		if index == _Ingredient
			AddIngredient(FLST, ModName)
		elseif index == _OreIngot
			AddOreIngot(FLST, ModName)
		elseif index == _AnimalHide
			AddAnimalHide(FLST, ModName)
		elseif index == _AnimalPart
			AddAnimalPart(FLST, ModName)
		elseif index == _Jewel
			AddJewel(FLST, ModName)
		elseif index == _Soulgem
			AddSoulgem(FLST, ModName)
		elseif index == _Drink
			AddDrink(FLST, ModName)
		elseif index == _Food
			AddFood(FLST, ModName)
		elseif index == _Potion
			AddPotion(FLST, ModName)
		elseif index == _FireWood
			AddFireWood(FLST, ModName)
		elseif index == _Clutter
			AddClutter(FLST, ModName)
		elseif index == _Scroll
			AddScroll(FLST, ModName)
		endif
	endWhile
endFunction

Function AddIngredient(Formlist List, String s)
	if s == "DG"
		List.AddForm(GetFormFromFile(0x185FB, "Dawnguard.esm"))		;INGR (DLC01PoisonBloom)
		List.AddForm(GetFormFromFile(0x183B7, "Dawnguard.esm"))		;INGR (DLC01ChaurusHunterAntennae)
		List.AddForm(GetFormFromFile(0x0B097, "Dawnguard.esm"))		;INGR (DLC01GlowPlant01Ingredient)
		List.AddForm(GetFormFromFile(0x059BA, "Dawnguard.esm"))		;INGR (DLC01MothWingAncestor)
		List.AddForm(GetFormFromFile(0x02A78, "Dawnguard.esm"))		;INGR (DLC1MountainFlower01Yellow)
	elseif s == "DB"
		List.AddForm(GetFormFromFile(0x3CD8E, "Dragonborn.esm"))	;INGR (DLC2TernFeathers)
		List.AddForm(GetFormFromFile(0x1FF75, "Dragonborn.esm"))	;INGR (DLC2HangingMoss)
		List.AddForm(GetFormFromFile(0x1CD74, "Dragonborn.esm"))	;INGR (DLC2AshCreepCluster)
		List.AddForm(GetFormFromFile(0x1CD72, "Dragonborn.esm"))	;INGR (DLC2NetchJelly)
		List.AddForm(GetFormFromFile(0x1CD71, "Dragonborn.esm"))	;INGR (DLC2AshHopperJelly)
		List.AddForm(GetFormFromFile(0x1CD6F, "Dragonborn.esm"))	;INGR (DLC2BoarTusk)
		List.AddForm(GetFormFromFile(0x1CD6E, "Dragonborn.esm"))	;INGR (DLC2BurntSprigganWood)
		List.AddForm(GetFormFromFile(0x1CD6D, "Dragonborn.esm"))	;INGR (DLC2GhoulAsh)
		List.AddForm(GetFormFromFile(0x17E97, "Dragonborn.esm"))	;INGR (DLC2Scathecraw01)
		List.AddForm(GetFormFromFile(0x17008, "Dragonborn.esm"))	;INGR (DLC2TramaRoot01)
		List.AddForm(GetFormFromFile(0x16E26, "Dragonborn.esm"))	;INGR (DLC2SpikyGrassAsh01)
	elseif s == "HF"
		List.AddForm(GetFormFromFile(0x0F1CC, "HearthFires.esm"))	;INGR (BYOHHawkEgg01)
		List.AddForm(GetFormFromFile(0x03545, "HearthFires.esm"))	;INGR (BYOHSalmonRoe01)
	elseif s == "101BUG"
		List.AddForm(GetFormFromFile(0x012E4, s101BugsName))		;INGR (00BloodyRed_; INGRed)
		List.AddForm(GetFormFromFile(0x012E6, s101BugsName))		;INGR (00DF_Pink_; INGRed)
		List.AddForm(GetFormFromFile(0x012E8, s101BugsName))		;INGR (00DF_Sunrise_; INGRed)
		List.AddForm(GetFormFromFile(0x0184F, s101BugsName))		;INGR (00DF_White_; INGRed)
		List.AddForm(GetFormFromFile(0x01851, s101BugsName))		;INGR (00DF_Yellow_; INGRed)
		List.AddForm(GetFormFromFile(0x01853, s101BugsName))		;INGR (00DF_Blue2_; INGRed)
		List.AddForm(GetFormFromFile(0x01855, s101BugsName))		;INGR (00DF_Green_; INGRed)
		List.AddForm(GetFormFromFile(0x01857, s101BugsName))		;INGR (00DF_Jungle_; INGRed)
		List.AddForm(GetFormFromFile(0x01859, s101BugsName))		;INGR (00DF_Ocean_; INGRed)
		List.AddForm(GetFormFromFile(0x0185B, s101BugsName))		;INGR (00DF_Purple_; INGRed)
		List.AddForm(GetFormFromFile(0x0185D, s101BugsName))		;INGR (00TB_Ladybird_; INGRed)
		List.AddForm(GetFormFromFile(0x0185F, s101BugsName))		;INGR (00TB_Firefly_; INGRed)
		List.AddForm(GetFormFromFile(0x01860, s101BugsName))		;INGR (00Harbinger_; INGRed)
		List.AddForm(GetFormFromFile(0x01863, s101BugsName))		;INGR (00PinkLuna_; INGRed)
		List.AddForm(GetFormFromFile(0x01864, s101BugsName))		;INGR (00BlueLuna_; INGRed)
		List.AddForm(GetFormFromFile(0x01866, s101BugsName))		;INGR (00PinkPeacock_; INGRed)
		List.AddForm(GetFormFromFile(0x01868, s101BugsName))		;INGR (00Paradise_; INGRed)
		List.AddForm(GetFormFromFile(0x0186B, s101BugsName))		;INGR (00BlackLime_; INGRed)
		List.AddForm(GetFormFromFile(0x0186C, s101BugsName))		;INGR (00DF_BloodyRed_; INGRed)
		List.AddForm(GetFormFromFile(0x0186E, s101BugsName))		;INGR (00GreenPeacock_; INGRed)
		List.AddForm(GetFormFromFile(0x01870, s101BugsName))		;INGR (00Purple_; INGRed)
		List.AddForm(GetFormFromFile(0x01872, s101BugsName))		;INGR (00JadeFreenLady_; INGRed)
		List.AddForm(GetFormFromFile(0x01875, s101BugsName))		;INGR (00MintZebra_; INGRed)
		List.AddForm(GetFormFromFile(0x01876, s101BugsName))		;INGR (00PinkZebra_; INGRed)
		List.AddForm(GetFormFromFile(0x01878, s101BugsName))		;INGR (00Lilac_; INGRed)
		List.AddForm(GetFormFromFile(0x0187A, s101BugsName))		;INGR (00BlueGold_; INGRed)
		List.AddForm(GetFormFromFile(0x03383, s101BugsName))		;INGR (00Lacerasus_; INGRed)
		List.AddForm(GetFormFromFile(0x03384, s101BugsName))		;INGR (00DF_Grim_; INGRed)
	elseif s == "IFish"
; 		List.AddForm(GetFormFromFile(0x106E1C, sImprovedFishName))		;INGR (CritterPondFish01Ingredient)
; 		List.AddForm(GetFormFromFile(0x106E1B, sImprovedFishName))		;INGR (CritterPondFish02Ingredient)
; 		List.AddForm(GetFormFromFile(0x106E1A, sImprovedFishName))		;INGR (CritterPondFish03Ingredient)
; 		List.AddForm(GetFormFromFile(0x106E19, sImprovedFishName))		;INGR (CritterPondFish04Ingredient)
; 		List.AddForm(GetFormFromFile(0x106E18, sImprovedFishName))		;INGR (CritterPondFish05Ingredient)
		List.AddForm(GetFormFromFile(0x000D63, sImprovedFishName))		;INGR (CritterPondFish06Ingredient)
		List.AddForm(GetFormFromFile(0x00182B, sImprovedFishName))		;INGR (CritterPondFish07Ingredient)
		List.AddForm(GetFormFromFile(0x00182E, sImprovedFishName))		;INGR (CritterPondFish08Ingredient)
		List.AddForm(GetFormFromFile(0x001831, sImprovedFishName))		;INGR (CritterPondFish09Ingredient)
		List.AddForm(GetFormFromFile(0x001837, sImprovedFishName))		;INGR (CritterPondFish10Ingredient)
		List.AddForm(GetFormFromFile(0x00183B, sImprovedFishName))		;INGR (CritterPondFish11Ingredient)
		List.AddForm(GetFormFromFile(0x001DA1, sImprovedFishName))		;INGR (CritterPondFish12Ingredient)
		List.AddForm(GetFormFromFile(0x002307, sImprovedFishName))		;INGR (CritterPondFish13Ingredient)
		List.AddForm(GetFormFromFile(0x002308, sImprovedFishName))		;INGR (CritterPondFish14Ingredient)
		List.AddForm(GetFormFromFile(0x00230D, sImprovedFishName))		;INGR (CritterPondFish15Ingredient)
		List.AddForm(GetFormFromFile(0x002310, sImprovedFishName))		;INGR (CritterPondFish16Ingredient)
		List.AddForm(GetFormFromFile(0x002313, sImprovedFishName))		;INGR (CritterPondFish17Ingredient)
		List.AddForm(GetFormFromFile(0x00287D, sImprovedFishName))		;INGR (CritterPondFish18Ingredient)
		List.AddForm(GetFormFromFile(0x002882, sImprovedFishName))		;INGR (CritterPondFish19Ingredient)
		List.AddForm(GetFormFromFile(0x0038B7, sImprovedFishName))		;INGR (CritterPondFish20Ingredient)
		List.AddForm(GetFormFromFile(0x0038B8, sImprovedFishName))		;INGR (CritterPondFish21Ingredient)
		List.AddForm(GetFormFromFile(0x0038BA, sImprovedFishName))		;INGR (CritterPondFish22Ingredient)
		List.AddForm(GetFormFromFile(0x0038BC, sImprovedFishName))		;INGR (CritterPondFish23Ingredient)
		List.AddForm(GetFormFromFile(0x003E27, sImprovedFishName))		;INGR (CritterPondFish24Ingredient)
		List.AddForm(GetFormFromFile(0x003E28, sImprovedFishName))		;INGR (CritterPondFish25Ingredient)
		List.AddForm(GetFormFromFile(0x004393, sImprovedFishName))		;INGR (CritterPondFish26Ingredient)
		List.AddForm(GetFormFromFile(0x004394, sImprovedFishName))		;INGR (CritterPondFish27Ingredient)
		List.AddForm(GetFormFromFile(0x004396, sImprovedFishName))		;INGR (CritterPondFish28Ingredient)
		List.AddForm(GetFormFromFile(0x004398, sImprovedFishName))		;INGR (CritterPondFish29Ingredient)
		List.AddForm(GetFormFromFile(0x0043A0, sImprovedFishName))		;INGR (CritterPondFish30Ingredient)
		List.AddForm(GetFormFromFile(0x004906, sImprovedFishName))		;INGR (CritterPondFish31Ingredient)
		List.AddForm(GetFormFromFile(0x004E6D, sImprovedFishName))		;INGR (CritterPondFish32Ingredient)
		List.AddForm(GetFormFromFile(0x004E6E, sImprovedFishName))		;INGR (CritterPondFish33Ingredient)
		List.AddForm(GetFormFromFile(0x006F54, sImprovedFishName))		;INGR (CritterPondFish34Ingredient)
		List.AddForm(GetFormFromFile(0x006F58, sImprovedFishName))		;INGR (CritterPondFish35Ingredient)
		List.AddForm(GetFormFromFile(0x006F5B, sImprovedFishName))		;INGR (CritterPondFish36Ingredient)
		List.AddForm(GetFormFromFile(0x006F5E, sImprovedFishName))		;INGR (CritterPondFish37Ingredient)
		List.AddForm(GetFormFromFile(0x008A5D, sImprovedFishName))		;INGR (CritterPondFish38Ingredient)
		List.AddForm(GetFormFromFile(0x0250F9, sImprovedFishName))		;INGR (FishEyeball)
	endif
endFunction

function AddOreIngot(formlist List, String S)
	if S == "Skyrim"
	elseif s == "DB"
	elseif s == "HF"
; 		List.AddForm(GetFormFromFile(0x0A511,"HearthFires.esm"))	;ACTI (BYOHMineClay01Dirt01)
	endIf
endFunction

Function AddAnimalHide(Formlist List, String S)
	if S == "DG"
		List.AddForm(GetFormFromFile(0x1199A, "Dawnguard.esm"))		;MISC (DLC1SabreCatHide)
		List.AddForm(GetFormFromFile(0x11999, "Dawnguard.esm"))		;MISC (DLC1DeerHide)
	elseif S == "DB"
	elseif S == "HF"
	endif
endFunction

function AddAnimalPart(formlist List, String S)
	If S == "Skyrim"
		List.AddForm(GetFormFromFile(0x319E4, "Skyrim.esm"))	;MISC (BoneTrollSkull01) to animalparts
		List.AddForm(GetFormFromFile(0xAF5FD, "Skyrim.esm"))	;MISC (BoneHumanSkullFull) 
		List.AddForm(GetFormFromFile(0xF6767, "Skyrim.esm"))	;MISC (dunWaywardPassSkull01) 
	elseif S == "DG"
	elseif S == "DB"
	elseif S == "HF"
	endif
endFunction

function AddJewel(formlist List, String S)
	If S == "Skyrim"
	elseif S == "DG"
	elseif S == "DB"
	elseif S == "HF"
	endif
endFunction

function AddSoulgem(formlist List, String S)
	If S == "Skyrim"
		List.AddForm(GetFormFromFile(0x67181, "Skyrim.esm"))	;MISC (SoulGemPiece001) 
		List.AddForm(GetFormFromFile(0x67182, "Skyrim.esm"))	;MISC (SoulGemPiece002) 
		List.AddForm(GetFormFromFile(0x67183, "Skyrim.esm"))	;MISC (SoulGemPiece003) 
		List.AddForm(GetFormFromFile(0x67184, "Skyrim.esm"))	;MISC (SoulGemPiece004) 
		List.AddForm(GetFormFromFile(0x67185, "Skyrim.esm"))	;MISC (SoulGemPiece005) 
	elseif S == "DG"
	elseif S == "DB"
	elseif S == "HF"
	endif
endFunction

Function AddDrink(Formlist List, String s)
	if s == "DG"
; 		List.AddForm(GetFormFromFile(0x1391D, "Dawnguard.esm"))	;ALCH (DLC1RedwaterDenSkooma) 
	elseif s == "DB"
		List.AddForm(GetFormFromFile(0x207E5, "Dragonborn.esm"))	;ALCH (DLC2Flin) 
		List.AddForm(GetFormFromFile(0x3572F, "Dragonborn.esm"))	;ALCH (DLC2FoodAshfireMead) 
		List.AddForm(GetFormFromFile(0x248CE, "Dragonborn.esm"))	;ALCH (DLC2Matze) 
		List.AddForm(GetFormFromFile(0x24E0B, "Dragonborn.esm"))	;ALCH (DLC2RRF04Sujamma) 
		List.AddForm(GetFormFromFile(0x320DF, "Dragonborn.esm"))	;ALCH (DLC2RRFavor01EmberbrandWine) 
	elseif s == "HF"
		List.AddForm(GetFormFromFile(0x03534, "HearthFires.esm"))	;ALCH (BYOHFoodMilk) 
		List.AddForm(GetFormFromFile(0x03535, "HearthFires.esm"))	;ALCH (BYOHFoodWineBottle04) 
		List.AddForm(GetFormFromFile(0x03536, "HearthFires.esm"))	;ALCH (BYOHFoodWineBottle03) 
	endif
endFunction

Function AddFood(Formlist List, String s)
	if s == "DG"
	elseif s == "DB"
		List.AddForm(GetFormFromFile(0x206E7, "Dragonborn.esm"))	;ALCH (DLC2FoodAshYam)
		List.AddForm(GetFormFromFile(0x3D125, "Dragonborn.esm"))	;ALCH (DLC2FoodAshHopperLeg)
		List.AddForm(GetFormFromFile(0x3CF72, "Dragonborn.esm"))	;ALCH (DLC2FoodBoarMeatCooked)
		List.AddForm(GetFormFromFile(0x3CD5B, "Dragonborn.esm"))	;ALCH (DLC2FoodHorkerAshYamStew)
		List.AddForm(GetFormFromFile(0x3BD15, "Dragonborn.esm"))	;ALCH (DLC2FoodAshHopperMeat)
		List.AddForm(GetFormFromFile(0x3BD14, "Dragonborn.esm"))	;ALCH (DLC2FoodBoarMeat)
	elseif s == "HF"
		List.AddForm(GetFormFromFile(0x03538, "HearthFires.esm"))	;ALCH (BYOHFoodFlour)
		List.AddForm(GetFormFromFile(0x0353C, "HearthFires.esm"))	;ALCH (BYOHFoodButter)
		List.AddForm(GetFormFromFile(0x03540, "HearthFires.esm"))	;ALCH (BYOHFoodMudcrabLegs)
		List.AddForm(GetFormFromFile(0x7434B, "skyrim.esm"))	;ALCH (FoodPotatoSoup)
		List.AddForm(GetFormFromFile(0x0353D, "skyrim.esm"))	;ALCH (FoodPotatoSoup)
		List.AddForm(GetFormFromFile(0xF4320, "skyrim.esm"))	;ALCH (FoodElsweyrFondue)
		List.AddForm(GetFormFromFile(0xF431E, "skyrim.esm"))	;ALCH (FoodVegetableSoup)
		List.AddForm(GetFormFromFile(0xF431D, "skyrim.esm"))	;ALCH (FoodVenisonStew)
		List.AddForm(GetFormFromFile(0xF431B, "skyrim.esm"))	;ALCH (FoodCabbagePotatoSoup)
		List.AddForm(GetFormFromFile(0xF4315, "skyrim.esm"))	;ALCH (FoodHorkerStew)
		List.AddForm(GetFormFromFile(0xF4314, "skyrim.esm"))	;ALCH (FoodBeefStew)
		List.AddForm(GetFormFromFile(0xEBA01, "skyrim.esm"))	;ALCH (FoodAppleCabbageStew)
		List.AddForm(GetFormFromFile(0x64B3B, "skyrim.esm"))	;ALCH (FoodSalmonCooked01)
		List.AddForm(GetFormFromFile(0x03533, "HearthFires.esm"))	;ALCH (BYOHFoodAppleDumpling01)
		List.AddForm(GetFormFromFile(0x03537, "HearthFires.esm"))	;ALCH (BYOHFoodPotatoBread01A)
		List.AddForm(GetFormFromFile(0x03539, "HearthFires.esm"))	;ALCH (BYOHFoodJuniperBerryCrostata)
		List.AddForm(GetFormFromFile(0x0353A, "HearthFires.esm"))	;ALCH (BYOHFoodJazbayCrostata)
		List.AddForm(GetFormFromFile(0x0353B, "HearthFires.esm"))	;ALCH (BYOHFoodSnowberryCrostata)
		List.AddForm(GetFormFromFile(0x0353E, "HearthFires.esm"))	;ALCH (BYOHFoodClamChowder)
		List.AddForm(GetFormFromFile(0x0353F, "HearthFires.esm"))	;ALCH (BYOHFoodMudcrabLegsCooked)
		List.AddForm(GetFormFromFile(0x03541, "HearthFires.esm"))	;ALCH (BYOHFoodSalmonCooked02)
		List.AddForm(GetFormFromFile(0x117FF, "HearthFires.esm"))	;ALCH (BYOHFoodChickenDumpling01)
		List.AddForm(GetFormFromFile(0x11801, "HearthFires.esm"))	;ALCH (BYOHFoodLavenderDumpling01)
		List.AddForm(GetFormFromFile(0x009DB, "HearthFires.esm"))	;ALCH (BYOHFoodBraidedBread01)
		List.AddForm(GetFormFromFile(0x009DC, "HearthFires.esm"))	;ALCH (BYOHFoodGarlicBread01)
	elseif s == "IFish"
		List.AddForm(GetFormFromFile(0x008FC3, sImprovedFishName))		;ALCH (FoodAmago)
		List.AddForm(GetFormFromFile(0x008FC7, sImprovedFishName))		;ALCH (FoodAmurCatfish)
		List.AddForm(GetFormFromFile(0x009A8F, sImprovedFishName))		;ALCH (FoodAyu)
		List.AddForm(GetFormFromFile(0x009AAF, sImprovedFishName))		;ALCH (FoodBlueGill)
		List.AddForm(GetFormFromFile(0x00AAEE, sImprovedFishName))		;ALCH (FoodBocaccio)
		List.AddForm(GetFormFromFile(0x009A9B, sImprovedFishName))		;ALCH (FoodCarp)
		List.AddForm(GetFormFromFile(0x0084F7, sImprovedFishName))		;ALCH (FoodCharcoalHikel)
		List.AddForm(GetFormFromFile(0x009A9F, sImprovedFishName))		;ALCH (FoodCherrySalmon)
		List.AddForm(GetFormFromFile(0x009AA3, sImprovedFishName))		;ALCH (FoodDollyVarden)
		List.AddForm(GetFormFromFile(0x009A97, sImprovedFishName))		;ALCH (FoodFlatheadMullet)
		List.AddForm(GetFormFromFile(0x009A93, sImprovedFishName))		;ALCH (FoodHerabuna)
		List.AddForm(GetFormFromFile(0x009AA7, sImprovedFishName))		;ALCH (FoodLargeMouthBass)
		List.AddForm(GetFormFromFile(0x00A58A, sImprovedFishName))		;ALCH (FoodOrientalWeatherfish)
		List.AddForm(GetFormFromFile(0x009AAB, sImprovedFishName))		;ALCH (FoodRainbowTrout)
; 		List.AddForm(GetFormFromFile(0x065C9F, sImprovedFishName))		;ALCH (FoodSalmon)
		List.AddForm(GetFormFromFile(0x009ABB, sImprovedFishName))		;ALCH (FoodTanago)
		List.AddForm(GetFormFromFile(0x009AB7, sImprovedFishName))		;ALCH (FoodThreeSpinedStickleback)
		List.AddForm(GetFormFromFile(0x00A025, sImprovedFishName))		;ALCH (FoodWhiteSpottedChar)
		List.AddForm(GetFormFromFile(0x009ABF, sImprovedFishName))		;ALCH (FoodYellowfinGoby)
		List.AddForm(GetFormFromFile(0x00B055, sImprovedFishName))		;ALCH (FoodAmagoCooked)
		List.AddForm(GetFormFromFile(0x00B058, sImprovedFishName))		;ALCH (FoodAmurCatfishCooked)
		List.AddForm(GetFormFromFile(0x00B05B, sImprovedFishName))		;ALCH (FoodAyuCooked)
		List.AddForm(GetFormFromFile(0x00CB79, sImprovedFishName))		;ALCH (FoodBlueGillCooked)
		List.AddForm(GetFormFromFile(0x00D0DE, sImprovedFishName))		;ALCH (FoodBocaccioCooked)
		List.AddForm(GetFormFromFile(0x00D0E1, sImprovedFishName))		;ALCH (FoodCarpCooked)
		List.AddForm(GetFormFromFile(0x00D0E4, sImprovedFishName))		;ALCH (FoodCharcoalHikelCooked)
		List.AddForm(GetFormFromFile(0x00B053, sImprovedFishName))		;ALCH (FoodCherrySalmonCooked)
		List.AddForm(GetFormFromFile(0x00D0E6, sImprovedFishName))		;ALCH (FoodDollyVardenCooked)
		List.AddForm(GetFormFromFile(0x00D0E9, sImprovedFishName))		;ALCH (FoodFlatHeadMulletCooked)
		List.AddForm(GetFormFromFile(0x00D0ED, sImprovedFishName))		;ALCH (FoodHerabunaCooked)
		List.AddForm(GetFormFromFile(0x00D0EF, sImprovedFishName))		;ALCH (FoodLargeMouthBassCooked)
		List.AddForm(GetFormFromFile(0x00D0F2, sImprovedFishName))		;ALCH (FoodOrientalWeatherfishCooked)
		List.AddForm(GetFormFromFile(0x00D0F6, sImprovedFishName))		;ALCH (FoodRainbowTroutCooked)
; 		List.AddForm(GetFormFromFile(0x064B3B, sImprovedFishName))		;ALCH (FoodSalmonCooked)
		List.AddForm(GetFormFromFile(0x00D0F8, sImprovedFishName))		;ALCH (FoodTanagoCooked)
		List.AddForm(GetFormFromFile(0x00D0FB, sImprovedFishName))		;ALCH (FoodThreeSpinedSticklebackCooked)
		List.AddForm(GetFormFromFile(0x00D0FF, sImprovedFishName))		;ALCH (FoodWhiteSpottedCharCooked)
		List.AddForm(GetFormFromFile(0x00D664, sImprovedFishName))		;ALCH (FoodYellowfinGobyCooked)
	endif
endFunction

Function AddPotion(Formlist List, String s)
	if s == "DG"
		List.AddForm(GetFormFromFile(0x18EF3, "Dawnguard.esm"))		;ALCH (DLC1BloodPotion)
		List.AddForm(GetFormFromFile(0x15A1E, "Dawnguard.esm"))		;ALCH (DLC1FoodSoulHuskExtract)
		List.AddForm(GetFormFromFile(0x1391D, "Dawnguard.esm"))		;ALCH (DLC1RedwaterDenSkooma)
	elseif s == "DB"
		List.AddForm(GetFormFromFile(0x390E0, "Dragonborn.esm"))	;ALCH (DLC2dunBloodskalPotionOfWaterWalking)
		List.AddForm(GetFormFromFile(0x1AAE2, "Dragonborn.esm"))	;ALCH (DLC2RestoreAll06)
		List.AddForm(GetFormFromFile(0x1AAE1, "Dragonborn.esm"))	;ALCH (DLC2RestoreAll05)
		List.AddForm(GetFormFromFile(0x1AAE0, "Dragonborn.esm"))	;ALCH (DLC2RestoreAll04)
		List.AddForm(GetFormFromFile(0x1AADF, "Dragonborn.esm"))	;ALCH (DLC2RestoreAll03)
		List.AddForm(GetFormFromFile(0x1AADE, "Dragonborn.esm"))	;ALCH (DLC2RestoreAll02)
		List.AddForm(GetFormFromFile(0x1AADD, "Dragonborn.esm"))	;ALCH (DLC2RestoreAll01)
	elseif s == "HF"
	endif
endFunction

Function AddPoison(Formlist List, String s)
	if s == "DG"
	elseif s == "DB"
		List.AddForm(GetFormFromFile(0x1CD7D, "Dragonborn.esm"))	;ALCH (DLC2NetchPoison)
	elseif s == "HF"
	endif
endFunction

Function AddFireWood(Formlist List, String s)
	if s == "DG"
	elseif s == "DB"
	elseif s == "HF"
	endif
endFunction

function AddClutter(formlist List, String S)
	If S == "Skyrim"
		List.AddForm(GetFormFromFile(0x12FDF, "Skyrim.esm"))	;MISC (Bucket01) 
		List.AddForm(GetFormFromFile(0x12FE6, "Skyrim.esm"))	;MISC (Bucket01) 
		List.AddForm(GetFormFromFile(0x12FE7, "Skyrim.esm"))	;MISC (Basket01) 
		List.AddForm(GetFormFromFile(0x12FE8, "Skyrim.esm"))	;MISC (Basket02) 
		List.AddForm(GetFormFromFile(0x12FE9, "Skyrim.esm"))	;MISC (Basket03) 
		List.AddForm(GetFormFromFile(0x12FEA, "Skyrim.esm"))	;MISC (Basket04) 
		List.AddForm(GetFormFromFile(0x12FEB, "Skyrim.esm"))	;MISC (Basket05) 
		List.AddForm(GetFormFromFile(0x12FEC, "Skyrim.esm"))	;MISC (BasketClosed01) 
		List.AddForm(GetFormFromFile(0x20949, "Skyrim.esm"))	;MISC (DBTortureTools) 
		List.AddForm(GetFormFromFile(0x318EC, "Skyrim.esm"))	;MISC (Lantern01) 
		List.AddForm(GetFormFromFile(0x318FA, "Skyrim.esm"))	;MISC (CastIronPotSmall01) 
		List.AddForm(GetFormFromFile(0x318FB, "Skyrim.esm"))	;MISC (CastIronPotMedium01) 
		List.AddForm(GetFormFromFile(0x31941, "Skyrim.esm"))	;MISC (BasicWoodenPlate01) 
		List.AddForm(GetFormFromFile(0x3199A, "Skyrim.esm"))	;MISC (BasicWoodenBowl01) 
		List.AddForm(GetFormFromFile(0x319E3, "Skyrim.esm"))	;MISC (BasicTankard01) 
		List.AddForm(GetFormFromFile(0x319E5, "Skyrim.esm"))	;MISC (WoodenLadle01) 
		List.AddForm(GetFormFromFile(0x33760, "Skyrim.esm"))	;MISC (Charcoal) 
		List.AddForm(GetFormFromFile(0x33761, "Skyrim.esm"))	;MISC (PaperRoll) 
		List.AddForm(GetFormFromFile(0x34CCE, "Skyrim.esm"))	;MISC (RuinsScalpel) 
		List.AddForm(GetFormFromFile(0x34CD0, "Skyrim.esm"))	;MISC (RuinsScrewTool) 
		List.AddForm(GetFormFromFile(0x34CD2, "Skyrim.esm"))	;MISC (RuinsEmbalmingPick) 
		List.AddForm(GetFormFromFile(0x34CD4, "Skyrim.esm"))	;MISC (RuinsScissor) 
		List.AddForm(GetFormFromFile(0x34CD6, "Skyrim.esm"))	;MISC (RuinsLinenPile01) 
		List.AddForm(GetFormFromFile(0x3807B, "Skyrim.esm"))	;MISC (Spigot01) 
		List.AddForm(GetFormFromFile(0x44E70, "Skyrim.esm"))	;MISC (Flagon) 
		List.AddForm(GetFormFromFile(0x4C3C6, "Skyrim.esm"))	;MISC (Inkwell01) 
		List.AddForm(GetFormFromFile(0x4C3C8, "Skyrim.esm"))	;MISC (Quill01) 
		List.AddForm(GetFormFromFile(0x6717F, "Skyrim.esm"))	;MISC (Broom01) 
		List.AddForm(GetFormFromFile(0x67180, "Skyrim.esm"))	;MISC (ClothesIron) 
		List.AddForm(GetFormFromFile(0x747FB, "Skyrim.esm"))	;MISC (Bucket02a) 
		List.AddForm(GetFormFromFile(0x747FE, "Skyrim.esm"))	;MISC (Bucket02b) 
		List.AddForm(GetFormFromFile(0x8632C, "Skyrim.esm"))	;MISC (Glazed02Jug01) 
		List.AddForm(GetFormFromFile(0x8F997, "Skyrim.esm"))	;MISC (StatueDibellaGold) 
		List.AddForm(GetFormFromFile(0x98620, "Skyrim.esm"))	;MISC (SilverGoblet01) 
		List.AddForm(GetFormFromFile(0x98621, "Skyrim.esm"))	;MISC (SilverGoblet02) 
		List.AddForm(GetFormFromFile(0x98623, "Skyrim.esm"))	;MISC (SilverJug01) 
		List.AddForm(GetFormFromFile(0x98624, "Skyrim.esm"))	;MISC (SilverPlate01) 
		List.AddForm(GetFormFromFile(0x98625, "Skyrim.esm"))	;MISC (SilverPlatter01) 
		List.AddForm(GetFormFromFile(0x98626, "Skyrim.esm"))	;MISC (SilverBowl01) 
		List.AddForm(GetFormFromFile(0x98627, "Skyrim.esm"))	;MISC (SilverBowl02) 
		List.AddForm(GetFormFromFile(0x9BAF8, "Skyrim.esm"))	;MISC (Basket06) 
		List.AddForm(GetFormFromFile(0xAA03E, "Skyrim.esm"))	;MISC (TGTQ01Candlestick) 
		List.AddForm(GetFormFromFile(0xABD30, "Skyrim.esm"))	;MISC (BasketCarry) 
		List.AddForm(GetFormFromFile(0xB08C7, "Skyrim.esm"))	;MISC (dunUniqueBeeInJar) 
		List.AddForm(GetFormFromFile(0xB9BCC, "Skyrim.esm"))	;MISC (GlazedBowl01) 
		List.AddForm(GetFormFromFile(0xB9BD0, "Skyrim.esm"))	;MISC (GlazedPlate01) 
		List.AddForm(GetFormFromFile(0xB9BD2, "Skyrim.esm"))	;MISC (GlazedPot01) 
		List.AddForm(GetFormFromFile(0xB9BD4, "Skyrim.esm"))	;MISC (GlazedPot02) 
		List.AddForm(GetFormFromFile(0xB9BD6, "Skyrim.esm"))	;MISC (GlazedJugLarge01) 
		List.AddForm(GetFormFromFile(0xB9BD8, "Skyrim.esm"))	;MISC (GlazedBowl02) 
		List.AddForm(GetFormFromFile(0xB9BDA, "Skyrim.esm"))	;MISC (GlazedGoblet01) 
		List.AddForm(GetFormFromFile(0xB9BDC, "Skyrim.esm"))	;MISC (GlazedCup01) 
		List.AddForm(GetFormFromFile(0xB9BDE, "Skyrim.esm"))	;MISC (GlazedJugSmall01) 
		List.AddForm(GetFormFromFile(0xBFB09, "Skyrim.esm"))	;MISC (Coal01) 
		List.AddForm(GetFormFromFile(0xCC84D, "Skyrim.esm"))	;MISC (BloodyRags01) 
		List.AddForm(GetFormFromFile(0xCE70B, "Skyrim.esm"))	;MISC (RuinedBook) 
		List.AddForm(GetFormFromFile(0xD955A, "Skyrim.esm"))	;MISC (FlowerBasketDrop) 
		List.AddForm(GetFormFromFile(0xE2617, "Skyrim.esm"))	;MISC (BasicPlate01) 
		List.AddForm(GetFormFromFile(0xE2618, "Skyrim.esm"))	;MISC (BasicPlate02) 
		List.AddForm(GetFormFromFile(0xE3CB7, "Skyrim.esm"))	;MISC (BurnedBook01) 
		List.AddForm(GetFormFromFile(0xE42DF, "Skyrim.esm"))	;MISC (SilverCandleStick01Off) 
		List.AddForm(GetFormFromFile(0xE42E0, "Skyrim.esm"))	;MISC (SilverCandleStick02Off) 
		List.AddForm(GetFormFromFile(0xE42E1, "Skyrim.esm"))	;MISC (SilverCandleStick03Off) 
		List.AddForm(GetFormFromFile(0xE4897, "Skyrim.esm"))	;MISC (RuinedBook02) 
		List.AddForm(GetFormFromFile(0xF03F8, "Skyrim.esm"))	;MISC (dunAlftandEmptySkoomaBottle) 
		List.AddForm(GetFormFromFile(0xF08F1, "Skyrim.esm"))	;MISC (GlazedJugLarge01Nordic) 
		List.AddForm(GetFormFromFile(0xF08F3, "Skyrim.esm"))	;MISC (GlazedJugSmall01Nordic) 
		List.AddForm(GetFormFromFile(0xF08F5, "Skyrim.esm"))	;MISC (GlazedPlate01Nordic) 
		List.AddForm(GetFormFromFile(0xF08F6, "Skyrim.esm"))	;MISC (GlazedPot01Nordic) 
		List.AddForm(GetFormFromFile(0xF08F7, "Skyrim.esm"))	;MISC (GlazedPot02Nordic) 
		List.AddForm(GetFormFromFile(0xF08F8, "Skyrim.esm"))	;MISC (GlazedCup01Nordic) 
		List.AddForm(GetFormFromFile(0xF08F9, "Skyrim.esm"))	;MISC (GlazedGoblet01Nordic) 
		List.AddForm(GetFormFromFile(0xF08FA, "Skyrim.esm"))	;MISC (GlazedBowl01Nordic) 
		List.AddForm(GetFormFromFile(0xF08FB, "Skyrim.esm"))	;MISC (GlazedBowl02Nordic) 
		List.AddForm(GetFormFromFile(0xF2012, "Skyrim.esm"))	;MISC (WineBottle01AEmpty) 
		List.AddForm(GetFormFromFile(0xF2013, "Skyrim.esm"))	;MISC (WineBottle01BEmpty) 
		List.AddForm(GetFormFromFile(0xF2014, "Skyrim.esm"))	;MISC (WineBottle02AEmpty) 
		List.AddForm(GetFormFromFile(0xF2015, "Skyrim.esm"))	;MISC (WineBottle02BEmpty) 
		List.AddForm(GetFormFromFile(0xF5D05, "Skyrim.esm"))	;MISC (Shovel01) 
		List.AddForm(GetFormFromFile(0xF5D06, "Skyrim.esm"))	;MISC (Shovel02) 
		List.AddForm(GetFormFromFile(0xF5D07, "Skyrim.esm"))	;MISC (Pitchfork001) 
		List.AddForm(GetFormFromFile(0xF5D08, "Skyrim.esm"))	;MISC (Pitchfork002) 
		List.AddForm(GetFormFromFile(0xF5D0A, "Skyrim.esm"))	;MISC (Saw01) 
		List.AddForm(GetFormFromFile(0xFBC3A, "Skyrim.esm"))	;MISC (dunUniqueFireflyInJar) 
		List.AddForm(GetFormFromFile(0xFBC3B, "Skyrim.esm"))	;MISC (dunUniqueDragonflyInJar) 
		List.AddForm(GetFormFromFile(0xFBC3C, "Skyrim.esm"))	;MISC (dunUniqueButterflyInJar) 
		List.AddForm(GetFormFromFile(0xFBC3D, "Skyrim.esm"))	;MISC (dunUniqueMothInJar) 
		List.AddForm(GetFormFromFile(0xFED17, "Skyrim.esm"))	;MISC (WineSolitudeSpicedBottleEmpty) 
	endIf
endFunction

Function AddScroll(Formlist List, String s)
	if s == "DG"
		List.AddForm(GetFormFromFile(0x14042, "Dawnguard.esm"))		;SCRL (DLC1dunRedwaterDenTelekinesisScroll)
	elseif s == "DB"
		List.AddForm(GetFormFromFile(0x331A1, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderEmptyScroll05)
		List.AddForm(GetFormFromFile(0x331A0, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderEmptyScroll04)
		List.AddForm(GetFormFromFile(0x3319F, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderEmptyScroll03)
		List.AddForm(GetFormFromFile(0x3319E, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderEmptyScroll02)
		List.AddForm(GetFormFromFile(0x274A5, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderPackmuleScroll)
		List.AddForm(GetFormFromFile(0x2749D, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderOilScroll)
		List.AddForm(GetFormFromFile(0x27490, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderGlowingScroll)
		List.AddForm(GetFormFromFile(0x20961, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderShockJumpingScroll)
		List.AddForm(GetFormFromFile(0x20960, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderShockCloakScroll)
		List.AddForm(GetFormFromFile(0x2095F, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderShockBombScroll)
		List.AddForm(GetFormFromFile(0x206DB, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderFrostJumpingScroll)
		List.AddForm(GetFormFromFile(0x206D9, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderFrostCloakScroll)
		List.AddForm(GetFormFromFile(0x206D3, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderFrostBombScroll)
		List.AddForm(GetFormFromFile(0x1DA03, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderEmptyScroll01)
		List.AddForm(GetFormFromFile(0x1CAB0, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderPoisonCloakScroll)
		List.AddForm(GetFormFromFile(0x19534, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderFireBombScroll)
		List.AddForm(GetFormFromFile(0x1952C, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderFireCloakScroll)
		List.AddForm(GetFormFromFile(0x1707B, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderPoisonBombScroll)
		List.AddForm(GetFormFromFile(0x16E1C, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderZombieScroll)
		List.AddForm(GetFormFromFile(0x14480, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderFireJumpingScroll)
		List.AddForm(GetFormFromFile(0x1445E, "Dragonborn.esm"))	;SCRL (DLC2ExpSpiderPoisonJumpingScroll)
	elseif s == "HF"
	endif
endFunction