Scriptname aaatowMB2_MCM_Quest extends SKI_ConfigBase  

aaatowMB2_Var_Quest Property MB2 Auto

int[] MBID_P1
int[] MBID_P2

bool bUserlistInit
int[] MBID_UL
bool[] StateUL

String[] MBContainerSet

int function GetVersion()
	return 2
endFunction

Event OnVersionUpdate(int a_version)
	if (a_version >= 2 && CurrentVersion < 2)
		OnConfigInit()
	endif
endEvent


Event OnConfigInit()
	ModName = "$MSSName"

	Pages = New String[3]
	Pages[0] = "$Basic1"
	Pages[1] = "$Basic2"
	Pages[2] = "$Userlist"

	MBID_P1 = new Int[20]
	MBID_P2 = new Int[20]

	MBContainerSet = new string[31]
	MBContainerSet[0] = "$NoChest"
	MBContainerSet[1] = "$DefaultChest"
endEvent

Event OnConfigOpen()
	bUserlistInit = true
endEvent

Event OnPageReset(String a_Page)
	If a_Page == ""
		LoadCustomContent("towawot/towMB2.dds")
		Return
	Else
		UnloadCustomContent()
	EndIf
	
	if a_Page == "$Basic1"
		AddHeaderOption("$Feature")
		AddEmptyOption()
		MBID_P1[0]	= AddToggleOption("$CraftAutoTake", MB2.State1[0])
		MBID_P1[1]	= AddToggleOption("$PickupAutoPutIn", MB2.State1[1])
		MBID_P1[2]	= AddToggleOption("$VendorAutoTake", MB2.State1[2])
		MBID_P1[3]	= AddToggleOption("$WeightCalculation", MB2.State1[3])

		AddEmptyOption()
		AddEmptyOption()
		
		AddHeaderOption("$Powers")
		AddEmptyOption()
		MBID_P1[5]	= AddToggleOption("$PutInPower", MB2.State1[5])
		MBID_P1[6]	= AddToggleOption("$TakeOutPower", MB2.State1[6])
		MBID_P1[7]	= AddToggleOption("$ContainerPower", MB2.State1[7])
		AddEmptyOption()

		AddEmptyOption()
		AddEmptyOption()

		AddHeaderOption("$Userlist")
		AddEmptyOption()
		MBID_P1[9]	= AddToggleOption("$AllowUserlist", MB2.State1[9])
		MBID_P1[10]	= AddKeyMapOption("$UserlistCode", MB2.State1[10])

		AddEmptyOption()
		AddEmptyOption()

	elseif a_Page == "$Basic2"
		AddHeaderOption("$Container")
		AddEmptyOption()
		SetCursorFillMode(TOP_TO_BOTTOM)
; 		======================== LEFT ========================
		MBID_P2[0]	= AddMenuOption("$VendorItemIngredient", MBContainerSet[MB2.State2[0]])
		MBID_P2[1]	= AddMenuOption("$VendorItemOreIngot", MBContainerSet[MB2.State2[1]])
		MBID_P2[2]	= AddMenuOption("$VendorItemAnimalHide", MBContainerSet[MB2.State2[2]])
		MBID_P2[3]	= AddMenuOption("$VendorItemAnimalPart", MBContainerSet[MB2.State2[3]])
		MBID_P2[4]	= AddMenuOption("$VendorItemGem", MBContainerSet[MB2.State2[4]])
		MBID_P2[5]	= AddMenuOption("$VendorItemSoulGem", MBContainerSet[MB2.State2[5]])

; 		======================== RIGHT ========================
		SetCursorPosition(3)
		MBID_P2[6]	= AddMenuOption("$VendorItemDrink", MBContainerSet[MB2.State2[6]])
		MBID_P2[7]	= AddMenuOption("$VendorItemFood", MBContainerSet[MB2.State2[7]])
		MBID_P2[8]	= AddMenuOption("$VendorItemPotion", MBContainerSet[MB2.State2[8]])
		MBID_P2[9]	= AddMenuOption("$VendorItemPoison", MBContainerSet[MB2.State2[9]])
		MBID_P2[10] = AddMenuOption("$VendorItemFireword", MBContainerSet[MB2.State2[10]])
		MBID_P2[11] = AddMenuOption("$VendorItemClutter", MBContainerSet[MB2.State2[11]])
		MBID_P2[12] = AddMenuOption("$VendorItemScroll", MBContainerSet[MB2.State2[12]])
		MBID_P2[13] = AddMenuOption("$Userlist", MBContainerSet[MB2.State2[13]])
	elseif a_Page == "$Userlist"
		if bUserlistInit
			MBID_UL = new int[128]
			StateUL = new bool[128]
			bUserlistInit = False
		endif
		ShowUserlist(MBID_UL, StateUL)
	endif
endEvent

Function ShowUserlist(int[] iOID, bool[] bState)
	string stName
	int index = 0
	int max = MB2.flUserlist[0].GetSize()

	if max > 0
		While index < max
			stName = MB2.flUserlist[0].Getat(index).getName()
			iOID[index] = AddToggleOption(stName, bState[index])
			index += 1
		endWhile
	else
		iOID[index] = AddToggleOption("$NotRegistered", True)
	endif
endFunction

Function SetUserlist(bool[] bState)
	int index = 0
	int max = MB2.flUserlist[0].GetSize()
	if max > 0
		if max > 128
			max = 127
		endif
		MB2.flUserlist[1].revert()
		if CopyList(MB2.flUserlist)
			MB2.flUserlist[0].revert()
			While index < max
				if !bState[index] 
					MB2.flUserlist[0].addform(MB2.flUserlist[1].getat(index))
				endif
				index += 1
			endWhile
		endif
	endif
endFunction

bool Function CopyList(FormList[] list)
	bool bResult
	int index = 0
	int max = list[0].GetSize()
	if max > 128
		max = 127
	endif
	
	while index <= max
		list[1].AddForm(list[0].GetAt(index))
		index += 1
		bResult = true
	endWhile
	return bResult
EndFunction

Event OnConfigClose()
; 元々AbilitySwitchとQuestSwitchはひとつの関数にする予定だった為、引数を態々積んで処理している

	Form[] formarr
	formarr = new form[1]
	formarr[0] = MB2.flControlSpells.GetAt(0) as form
	AbilitySwitch(formarr, MB2.State1[5])

	formarr[0] = MB2.flControlSpells.GetAt(1) as form
	AbilitySwitch(formarr, MB2.State1[6])

	formarr[0] = MB2.flControlSpells.GetAt(2) as form
	AbilitySwitch(formarr, MB2.State1[7])

	formarr[0] = MB2.ActivateCraftFurniture as form
	AbilitySwitch(formarr, MB2.State1[0])
	
	RegisterForModEvent("MCMClose", "OnMCMClose")
	SendModEvent("MCMClose")
endEvent

Event OnMCMClose(string eventName, string strArg, float numArg, Form sender)
; MCMのイベント内では一部の関数を使うと処理が止まってしまう為
; 別のイベントを作って処理をしている（今回はstart()が該当する）

	Quest[] qstarr
	qstarr = new quest[1]
	qstarr[0] = MB2.AutoPutInQuest
	QuestSwitch(qstarr, MB2.State1[1])

	qstarr[0] = MB2.VendorQuest
	QuestSwitch(qstarr, MB2.State1[2])

	qstarr[0] = MB2.UserlistQuest
	QuestSwitch(qstarr, MB2.State1[9])

	qstarr = new quest[2]
	qstarr[0] = MB2.WeightCalculationQuest[0]
	qstarr[1] = MB2.WeightCalculationQuest[1]

	if !QuestSwitch(qstarr, MB2.State1[3])
		Weapon wpn = MB2.flWeightWeapon.Getat(0) as Weapon
		Game.GetPlayer().RemoveItem(wpn, 9999999, true)
	endif

	if !bUserlistInit
		SetUserlist(StateUL)
	endif
endEvent

bool Function AbilitySwitch(form[] akform, int stat)
	form Ability
	string sMode
	int index = akform.length
	bool bResult
	Actor aPlayer = Game.GetPlayer()
	
	while index
		index -=1
		if akform[index] as Spell
			sMode = "spell"
			Ability = akform[index] as form
			
		elseif akform[index] as Perk
			sMode = "perk"
			Ability = akform[index] as form
		endif

; ↑↓はひとつに纏められるはずなんだけど何かの理由で分けたっぽい

		if sMode == "spell"
			Spell abSpell = Ability as spell
			if stat && !aPlayer.HasSpell(AbSpell)
				aPlayer.AddSpell(abSpell)
				bResult = true
			elseif !stat && aPlayer.HasSpell(AbSpell)
				aPlayer.RemoveSpell(abSpell)
				bResult = false
			endif
		elseif sMode == "perk"
			Perk abPerk = Ability as perk
			if stat && !aPlayer.HasPerk(abPerk)
				aPlayer.AddPerk(abPerk)
				bResult = true
			elseif !stat && aPlayer.HasPerk(abPerk)
				aPlayer.RemovePerk(abPerk)
				bResult = false
			endif
		endif
	endWhile
	return bResult
endFunction

bool Function QuestSwitch(quest[] qst, int stat)
	int index = qst.length
	bool bResult

	while index
		index -=1
		if stat && !qst[index].IsRunning()
			qst[index].Start()
			bResult = true
		elseif stat && qst[index].IsRunning()
			qst[index].Stop()
			qst[index].Start()
			bResult = true
		elseif !stat && qst[index].IsRunning()
			qst[index].Stop()
			bResult = false
		endif
	endWhile
	return bResult
endFunction

; Function SetGV()
; 	int index = 0
; 	int max = MB2.flGVFLST.GetSize()
; 	While index < max
; 		Utility.wait(0.0001)
; 		(MB2.flGVFLST.GetAt(index) as GlobalVariable).SetValue(MB2.State2[index] as float)
; 		index += 1
; 	endWhile
; endFunction

Event OnOptionSelect(int a_option)
	if		ChangeToggle(a_option, MBID_P1, MB2.State1)
	elseif	ChangeToggle(a_option, MBID_P2, MB2.State2)
	elseif 	ChangeToggleBool(a_option, MBID_UL, StateUL)
	endif
endEvent

bool function ChangeToggle(int Option, int[] ID, int[] States)
	bool bResult = false
	int index = ID.find(Option)
	if index != -1
		States[index] = (!(States[index] as bool)) as int
		SetToggleOptionValue(Option, States[index])
		bResult = true
	endif
	return bResult
endFunction

bool function ChangeToggleBool(int Option, int[] ID, bool[] States)
	bool bResult = false
	int index = ID.find(Option)
	if index != -1
		States[index] = !(States[index] as bool)
		SetToggleOptionValue(Option, States[index])
		bResult = true
	endif
	return bResult
endFunction

event OnOptionMenuOpen(int a_option)
	int index = MBID_P2.find(a_option)
	if index != -1
		SetMenuDialogOptions(MBContainerSet)
		SetMenuDialogStartIndex(MB2.State2[index])
		SetMenuDialogDefaultIndex(1)
		return
	endif
endEvent

event OnOptionMenuAccept(int a_option, int Optionindex)
	int index = MBID_P2.find(a_option)
	if index != -1
		MB2.State2[index] = Optionindex
		SetMenuOptionValue(MBID_P2[index], MBContainerSet[MB2.State2[index]])
		return
	endif
endEvent

event OnOptionKeyMapChange(int a_option, int keyCode, string conflictControl, string conflictName)
	if a_option == MBID_P1[10]
		SetKeyMapOptionValue(a_option, keyCode)
		MB2.State1[10] = keyCode
	endif
endEvent
