Scriptname aaatowMB2_Stop_Quest extends Quest  

Int count = 0
Event OnInit()
	if self.IsRunning()
		RegisterforSingleUpdate(2.0)
	endif
endEvent

Function SetCount()
	count += 1
	if count >= self.GetNumAliases()
		if self.IsRunning()
			Self.Stop()
		endif
	endif
endFunction

Event OnUpdate()
	if self.IsRunning()
		debug.Notification("MB2: Warning: Forced stop.")
		self.Stop()
	endif
endEvent
