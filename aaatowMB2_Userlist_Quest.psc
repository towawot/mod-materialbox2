Scriptname aaatowMB2_Userlist_Quest extends ReferenceAlias  

aaatowMB2_Var_Quest Property MB2 Auto

int[] HotKeys
Bool bHotKeyMode = False
Bool bEnableMenu = False
Bool bExclusionMode

int _Userlist = 0
int _Exclusionlist = 1

Event OnInit()
	OnPlayerLoadGame()
EndEvent

Event OnPlayerLoadGame()
	if !GetOwningQuest().IsRunning()
		return
	endif

	HotKeys = New Int[3]
	RegisterForMenu("ContainerMenu")
EndEvent

Event OnMenuOpen(string menuName)
	if menuName != "ContainerMenu"
		return
	endif

	bool bUserlist = MB2.State1[9] as bool
	if bUserlist
		HotKeys[_Userlist] = MB2.State1[10]
	else
		HotKeys[_Userlist] = 0
	endif
 	bool bExclusionlist = MB2.State1[11] as bool
 	if bExclusionlist
 		HotKeys[_Exclusionlist] = MB2.State1[12]
 	else
 		HotKeys[_Exclusionlist] = 0
 	endif

	if bUserlist && bExclusionlist
		if HotKeys[_Userlist] == HotKeys[_Exclusionlist] ; Message Menu mode
			bEnableMenu = True
			RegisterForKey(HotKeys[_Userlist])
		else
			RegisterForKey(HotKeys[_Userlist]) ; Userlist
			RegisterForKey(HotKeys[_Exclusionlist]) ; Exclusionlist
		endif
	else
		if bUserlist
			RegisterForKey(HotKeys[_Userlist]) ; Userlist
		elseif bExclusionlist
			RegisterForKey(HotKeys[_Exclusionlist]) ; Exclusionlist
		endif
	endif
endEvent

Event OnMenuClose(string menuName)
	if menuName != "ContainerMenu"
		return
	endif
	bHotKeyMode = False
	UnregisterForAllKeys()
endEvent

Event OnKeyDown(Int KeyCode)
	If KeyCode == HotKeys[_Userlist]
		bHotKeyMode = True
		bExclusionMode = False
	elseif KeyCode == HotKeys[_Exclusionlist]
		bHotKeyMode = True
		bExclusionMode = True
	else
		return
	EndIf
EndEvent

Event OnKeyUp(Int KeyCode, Float HoldTime)
	If KeyCode == HotKeys[_Userlist] || KeyCode == HotKeys[_Exclusionlist]
		bHotKeyMode = False
	else
		return
	EndIf
EndEvent

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	if !bHotKeyMode || !Utility.IsinMenumode() || !akSourceContainer
		bHotKeyMode = False
		return
	endif 

	Message MessageMenu
	int MessageSelect
	if bEnableMenu
		MessageMenu = MB2.flMessageMenu.Getat(0) as Message
		MessageSelect = MessageMenu.Show()
	elseif bExclusionMode
		MessageSelect = _Exclusionlist
	else
		MessageSelect = _Userlist
	endif

	if MessageSelect == _Userlist
		formlist list = MB2.flMaterial.Getat(13) as formlist
		if AddObject(list, akBaseItem)
			Debug.Notification("$RegisterUserlist")
		else
			Debug.Notification("$NotRegisterUserlist")
		endif
; 	elseif MessageSelect == _Exclusionlist
; 		if AddObject(MB2.flExclusionlistFLST, akBaseItem)
; 			MB2.MessageDisplay(16)
; 		endif
	else
		return
	endif
endEvent

Bool Function AddObject(formlist list , form fmObj)
	if MB2.flGold.hasForm(fmObj)
		Return false
	endif

	if !MB2.HasMaterialList(fmObj, true)
		list.AddForm(fmObj)
		Return true
	endif
	Return false
endFunction

Event OnReset()
	UnregisterForAllMenus()
	UnregisterForAllKeys()
endEvent