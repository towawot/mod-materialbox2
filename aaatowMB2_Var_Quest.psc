Scriptname aaatowMB2_Var_Quest extends Quest  Conditional

float fVersion = 0.8

Formlist Property flMaterial Auto
Formlist Property flControlSpells Auto
Formlist Property flWeightWeapon Auto
Formlist Property flDuplicateConfirm Auto
Formlist Property flGold Auto
Formlist Property flMessageMenu Auto
Formlist[] Property flUserlist Auto

Quest Property PutInQuest Auto
Quest Property TakeOutQuest Auto
Quest Property ContainerManagerQuest Auto
Quest Property AutoPutInQuest Auto
Quest[] Property WeightCalculationQuest Auto
Quest Property VendorQuest Auto
Quest Property UserlistQuest Auto
Quest Property CallContainerQuest Auto

Perk Property ActivateCraftFurniture Auto
Perk Property ContainerMenuPerk Auto
Spell Property MaxWeightIncrease Auto
; bool[] property bRunWeightAlias Auto

Int[] Property State1 Auto
Int[] Property State2 Auto

float fCurrent
Function Maintenance()
	if fCurrent < fVersion
; 		debug.Notification("MB2 Maintenance")
		State1 = new Int[20]
		State2 = new Int[20]
		State1[10] = 33
		fCurrent = fVersion
		ContainerManagerQuest.Stop()
		ContainerManagerQuest.Start()
	endif
endFunction

Bool Function HasMaterialList(Form akForm, bool bCurrent = true)
	int index = flMaterial.GetSize()
	formlist list
	while index
		index -= 1
		if bCurrent
			if State2[index]
				list = flMaterial.Getat(index) as formlist
				if list.HasForm(akForm)
					return true
				endif
			endif
		else
			list = flMaterial.Getat(index) as formlist
			if list.HasForm(akForm)
				return true
			endif
		endif
	endWhile
	return false
endFunction
