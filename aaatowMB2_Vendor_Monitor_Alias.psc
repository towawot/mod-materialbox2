Scriptname aaatowMB2_Vendor_Monitor_Alias extends ReferenceAlias  

aaatowMB2_Var_Quest Property MB2 Auto
Import Game
Import Utility

Event OnVendorTopicInfo(bool bStart)
	if bStart
		RegisterForMenu("BarterMenu")
		Game.GetPlayer().AddSpell(MB2.MaxWeightIncrease, False)
		MB2.TakeOutQuest.Start()
; 		debug.Notification("BarterMenu Open")
	endif
endEvent

Event OnMenuClose(String MenuName)
	if MenuName == "BarterMenu"
; 		debug.Notification("BarterMenu Close")
		MB2.PutInQuest.Start()
		WaitQuestClose(MB2.PutInQuest)
		Game.GetPlayer().RemoveSpell(MB2.MaxWeightIncrease)
		UnRegisterForMenu("BarterMenu")
	endif
endEvent

Function WaitQuestClose(Quest Qst)
	int index = 10
	bool bLoop = True

	wait(0.1)
	While Qst.IsRunning() && bLoop
		wait(0.1)
		index -= 1
		if index
			bLoop = False
		endif
	endWhile
endFunction

; Testing Events...
Event OnPlayerSneakEvent(bool bStart)
	debug.Notification("OnPlayerSneakEvent bStart = " + bStart)
endEvent
Event OnPlayerSwimEvent(bool bStart)
	debug.Notification("OnPlayerSwimEvent bStart = " + bStart)
	debug.trace("OnPlayerSwimEvent bStart = " + bStart)
endEvent
Event OnPlayerWeaponEquipEvent(bool bStart)
	debug.Notification("OnPlayerWeaponEquipEvent bStart = " + bStart)
	debug.trace("OnPlayerWeaponEquipEvent bStart = " + bStart)
endEvent
Event OnPlayerSpellStartEvent(form akform, int bDelivery, bool bConcentration, bool IsRightHand)
	debug.Notification("OnPlayerSpellStartEvent akform = " + akform.Getname() + " IsRightHand = " + IsRightHand + " bDelivery = " + bDelivery + " bConcentration = " + bConcentration)
	debug.trace("OnPlayerSpellStartEvent akform = " + akform.Getname() + " IsRightHand = " + IsRightHand + " bDelivery = " + bDelivery + " bConcentration = " + bConcentration)
endEvent
Event OnPlayerSpellReadyEvent(form akform, bool IsRightHand)
	debug.Notification("OnPlayerSpellReadyEvent akform = " + akform.Getname() + " IsRightHand = " + IsRightHand)
	debug.trace("OnPlayerSpellReadyEvent akform = " + akform.Getname() + " IsRightHand = " + IsRightHand)
endEvent
Event OnPlayerSpellReleaseEvent(form akform, bool IsRightHand)
	debug.Notification("OnPlayerSpellReleaseEvent akform = " + akform.Getname() + " IsRightHand = " + IsRightHand)
	debug.trace("OnPlayerSpellReleaseEvent akform = " + akform.Getname() + " IsRightHand = " + IsRightHand)
endEvent
